//File to control user login page
var username, password, login;
function redirector(){
	
	username = document.getElementById("username").value;
	password = document.getElementById("password").value;
	
	//check for login remember values
	if(document.getElementById('remember').checked){
		login = "remember";		
	} else {
		login = "no";
	}
	
		
		//login remember
		if(login === "remember" ){
			setCookie("username", username, 20);
			setCookie("password", password, 20);
		} else{
			//when login not remember
			setCookie("username", username, 0.1);
			setCookie("password", password, 0.1);
		}
        
        //rediection based on user or mechanic
			window.open("index.html", "_self");
			
		
}

function validate(){
		//take in the values
		username = document.getElementById("username").value;
		password = document.getElementById("password").value;
		var jsonObj = {"password":password, "username":username};
		
		//check for empty fields
		if(password == "" | username==""){
		alert("Please enter username and password");
	}else{
		
		//jquery code to check password
		$.ajax({
                type: "POST",
                url: "PHPValidate/CheckPassword.php",
				data: jsonObj,
				dataType: 'text',
                success: function(json_data){
                    var jsonObj = $.parseJSON(json_data);

                   if(password === jsonObj.password){
						alert("You are verified");
						document.getElementById("password").style.borderColor = "initial";
						redirector();
					}
					else{
						document.getElementById("password").style.borderColor = "red";
						document.getElementById("alert").innerHTML="please enter correct password";
					}
                }
            });
			
	}
/*			//ajax code to verify password
			var xmlhttp = new XMLHttpRequest();
			xmlhttp.onreadystatechange = function() {
				confirm("ready state = "+xmlhttp.readyState+ "status" + xmlhttp.status);
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					alert(xmlhttp.responseText);
					var jsonObj = JSON.parse(xmlhttp.responseText);
					if(password === jsonObj.password){
						alert("You are verified");
					}
					else{
						alert(jsonObj.password);
					}
				}
			};
		xmlhttp.open("GET", "CheckPassword.php?a=0", true);
		xmlhttp.send();
*/
}

//set the cookie in the browser
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}


//get the cookie stored in the browser
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}

/*
//checks for the cookie from home page and redirects to login in case of no cookie
function checkCookieHome() {
		var username=getCookie("username");
		if (username==""){
			window.open("index.html", "_self");
		}
}
//checks for the cookie from login page and redirects to home page when cookie is found
function checkCookieIndex() {
    var username=getCookie("username");
    if(username !== ""){
			window.open("home.html", "_self");
	}
}
*/

//deletes all the stored cookies
function deleteCookie(){
	document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
	document.cookie = "password=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
}

//used to log out
function logout(){
	var yes=confirm("Are you sure to logout?");
	console.log("inside logout main");
	if(yes===true){
		deleteCookie();
		window.open("index.html", "_self");
	}
}

//check status for remaining pages
function checkStatus(){
	var username=getCookie("username");
	if(username===""){
		//console.log("inside login funtion");
		document.getElementById("logout").style.display = "none";
		document.getElementById("login").style.display = "initial";
		document.getElementById("login").innerHTML="Login";
		
	}
	else{
		//console.log("inside logout funtion");
		document.getElementById("login").style.display = "none";
		document.getElementById("logout").style.display = "initial";
		document.getElementById("logut").innerHTML="Logout";
	}
}

//check status for login pages
function checkStatusLogin(){
	var username=getCookie("username");
	if(username!==""){
		//console.log("inside logout funtion");
		window.open("index.html", "_self");
	}
}