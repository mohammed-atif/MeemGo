
var map = document.getElementById("inputmap");
var latitude, longitude;
//main function to get location
function getLocation() {
	var map = document.getElementById("inputmap");
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
    } else { 
        map.innerHTML = "Geolocation is not supported by this browser.";
    }
}

//function to display the map
function showPosition(position) {
    lat = position.coords.latitude;
    lon = position.coords.longitude;
	latitude = lat;
	longitude = lon;
    latlon = new google.maps.LatLng(lat, lon);
    mapholder = document.getElementById('inputmap');
    mapholder.style.height = '230px';
    mapholder.style.width = '100%';
	mapholder.style.margin = 'auto';

    var myOptions = {
    center:latlon,zoom:14,
    mapTypeId:google.maps.MapTypeId.ROADMAP,
    mapTypeControl:false,
    navigationControlOptions:{style:google.maps.NavigationControlStyle.SMALL}
    }
    
    var map = new google.maps.Map(document.getElementById("inputmap"), myOptions);
    var marker = new google.maps.Marker({position:latlon,map:map,title:"You are here!"});
	
	google.maps.event.addListener(map, 'click', function(event) {
   // alert('Lat: ' + event.latLng.lat() + ' Lng: ' + event.latLng.lng());
	latitude=event.latLng.lat();
	longitude=event.latLng.lng();
	updateMap();

});
updateAddress();
}

function updateMap(){
	lat = latitude;
    lon = longitude;
	latlon = new google.maps.LatLng(lat, lon);
	var myOptions = {
    center:latlon,zoom:14,
    mapTypeId:google.maps.MapTypeId.ROADMAP,
    mapTypeControl:false,
    navigationControlOptions:{style:google.maps.NavigationControlStyle.SMALL}
    }
    
    var map = new google.maps.Map(document.getElementById("inputmap"), myOptions);
    var marker = new google.maps.Marker({position:latlon,map:map,title:"You are here!"});
	google.maps.event.addListener(map, 'click', function(event) {
   // alert('Lat: ' + event.latLng.lat() + ' Lng: ' + event.latLng.lng());
	latitude=event.latLng.lat();
	longitude=event.latLng.lng();
	updateMap();

});

updateAddress();
}

//funtion to show error
function showError(error) {
    switch(error.code) {
        case error.PERMISSION_DENIED:
            alert("User denied the request for Geolocation.");
            break;
        case error.POSITION_UNAVAILABLE:
            alert("Location information is unavailable.");
            break;
        case error.TIMEOUT:
            alert("The request to get user location timed out.");
            break;
        case error.UNKNOWN_ERROR:
            alert("An unknown error occurred.");
            break;
    }
}

/*
function putLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
    } else { 
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}
*/

function updateAddress(){
	var address_options = [];
	var alternates=document.getElementById('alternateAddress');
	var latlon="latlng="+latitude+","+longitude;
	var url="http://maps.googleapis.com/maps/api/geocode/json?";
	url+=latlon;
	//alert(latlon);
	$.ajax({
                type: "POST",
                url: url ,
				dataType: 'text',
                success: function(json_data){
					console.log(json_data);
                    var jsonObj = $.parseJSON(json_data);
					if(jsonObj.status==="OK"){
						var length=jsonObj.results.length;
						alternates.innerHTML="";
						for(i=0; i<length; i++){
							address_options[i]=jsonObj.results[i].formatted_address;
							var option=document.createElement('option');
							option.text=address_options[i];
							alternates.appendChild(option);
							console.log(jsonObj.results[i].formatted_address);
							console.log("address="+address_options[i]);
						}
						document.getElementById("userAddress").value = address_options[0];
					}
					else{
						alert("adress couldn't be retrieved");
					}
                }
            });
}

function addressUpdate(address){
	var e = document.getElementById("alternateAddress");
	document.getElementById('userAddress').value=e.options[e.selectedIndex].text;
}

